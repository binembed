#ifndef BINEMBED_H
#define BINEMBED_H 1
#include <stdint.h>
struct binembed_file {
  char const * const data;
  intptr_t const size;
};
struct binembed_node {
  char const * const name;
  enum { binembed_none = 0, binembed_file, binembed_dir } const type;
  union {
    void const * const none;
    struct binembed_file const * const file;
    struct binembed_node const * const dir;
  } const content;
};
#endif
