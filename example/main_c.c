#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Example.h"

#define SEP "/"

void printEmbedded(char const *path, struct binembed_node const *node) {
  int pathLength = 0;
  char *pathBuffer = 0;
  for (; node->type != binembed_none; ++node) {
    switch (node->type) {
      case binembed_file:
        pathLength = strlen(path) + strlen(SEP) + strlen(node->name);
        pathBuffer = malloc(pathLength + 1);
        if (! pathBuffer) {
          exit(1);
        }
        sprintf(pathBuffer, "%s%s%s", path, SEP, node->name);
        printf("\n\n%s\n", pathBuffer);
        for (int i = 0; i < pathLength; ++i) {
          printf("-");
        }
        printf("\n\n{{{\n");
        struct binembed_file const *f = node->content.file;
        fflush(stdout);
        if (1 != fwrite(f->data, f->size, 1, stdout)) {
          exit(1);
        }
        fflush(stdout);
        printf("}}}\n");
        free(pathBuffer);
        pathBuffer = 0;
        break;
      case binembed_dir:
        pathLength = strlen(path) + strlen(SEP) + strlen(node->name);
        pathBuffer = malloc(pathLength + 1);
        if (! pathBuffer) {
          exit(1);
        }
        sprintf(pathBuffer, "%s%s%s", path, SEP, node->name);
        printEmbedded(pathBuffer, node->content.dir);
        free(pathBuffer);
        pathBuffer = 0;
        break;
      default:
        break;
    }
  }
}

int main(int argc, char **argv) {
  printf("binembed-example\n");
  printf("================\n");
  printEmbedded("binembed-example", Example);
  return 0;
}
