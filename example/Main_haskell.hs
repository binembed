import Prelude hiding  (putStr)
import Data.ByteString (putStr, ByteString)
import Data.Map (toAscList)
import System.FilePath ((</>))
import Example

main :: IO ()
main = do
  putStrLn "binembed-example\n\
           \================"
  printEmbedded "binembed-example" =<< unBinEmbed example

printEmbedded :: String -> Node ByteString -> IO ()
printEmbedded path (File f) = do
  putStrLn "\n"
  putStrLn path
  putStrLn (replicate (length path) '-')
  putStrLn ""
  putStrLn "{{{"
  putStr f
  putStrLn "}}}"

printEmbedded path (Dir d) = do
  mapM_ (\(p, n) -> printEmbedded (path </> p) n) (toAscList d)
